function [J grad] = nnCostFunction(nn_params, ...
                                   input_layer_size, ...
                                   hidden_layer_size, ...
                                   num_labels, ...
                                   X, y, lambda)
%NNCOSTFUNCTION Implements the neural network cost function for a two layer
%neural network which performs classification
%   [J grad] = NNCOSTFUNCTON(nn_params, hidden_layer_size, num_labels, ...
%   X, y, lambda) computes the cost and gradient of the neural network. The
%   parameters for the neural network are "unrolled" into the vector
%   nn_params and need to be converted back into the weight matrices. 
% 
%   The returned parameter grad should be a "unrolled" vector of the
%   partial derivatives of the neural network.
%

% Reshape nn_params back into the parameters Theta1 and Theta2, the weight matrices
% for our 2 layer neural network
Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)), ...
                 hidden_layer_size, (input_layer_size + 1));

Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                 num_labels, (hidden_layer_size + 1));

% Setup some useful variables
m = size(X, 1);
         
% You need to return the following variables correctly 
J = 0;
Theta1_grad = zeros(size(Theta1));
Theta2_grad = zeros(size(Theta2));

% ====================== YOUR CODE HERE ======================
% Instructions: You should complete the code by working through the
%               following parts.
%
% Part 1: Feedforward the neural network and return the cost in the
%         variable J. After implementing Part 1, you can verify that your
%         cost function computation is correct by verifying the cost
%         computed in ex4.m
%

%Working Code without Vectorization
%c = [1:num_labels];
%X = [ones(m,1),X]; %append m bias unit to fron of a1;
%for i=1:m
%  a1 = X(i,:);
%  a2 = sigmoid(a1 * Theta1');
%  a2 = [ones(1,1),a2]; %add 1 bias unit to front of a2
%  a3 = sigmoid(a2*Theta2');
%  actY = (y(i) == c);
%  J = J - (log(a3)*actY' + (log(1-a3))*(1-actY)' );
%end;

%Working Code with complete Vectorization
c = [1:num_labels];
X = [ones(m,1),X]; %append m bias units to fronr of a1;
a1 = X;
a2 = sigmoid(a1 * Theta1');
a2 = [ones(m,1),a2]; %add m bias units to front of a2
a3 = sigmoid(a2*Theta2');
actY = (y == c);
mExamples= -(log(a3).*actY + (log(1-a3)).*(1-actY));
J = sum(mExamples(:));
J = J/m;

if lambda!=0
  Theta1WithoutBias = Theta1(:,2:end);
  Theta2WithoutBias = Theta2(:,2:end);
  theta1Squared = Theta1WithoutBias .* Theta1WithoutBias;
  theta2Squared = Theta2WithoutBias .* Theta2WithoutBias;
  J = J + (lambda/(2*m)) * (sum(theta1Squared(:)) + sum(theta2Squared(:))); 
endif;

% Part 2: Implement the backpropagation algorithm to compute the gradients
%         Theta1_grad and Theta2_grad. You should return the partial derivatives of
%         the cost function with respect to Theta1 and Theta2 in Theta1_grad and
%         Theta2_grad, respectively. After implementing Part 2, you can check
%         that your implementation is correct by running checkNNGradients
%
%         Note: The vector y passed into the function is a vector of labels
%               containing values from 1..K. You need to map this vector into a 
%               binary vector of 1's and 0's to be used with the neural network
%               cost function.
%
%         Hint: We recommend implementing backpropagation using a for-loop
%               over the training examples if you are implementing it for the 
%               first time.
%

%Working Code without Vectorization
%Theta1 dimension 25 * 401
%Theta2 dimension 10 * 26

%c = [1:num_labels];
%X = [ones(m,1),X]; %append m bias unit to fron of a1;

for i=1:m
  a1 = X(i,:); % (1 * 401)
  z2 = a1 * Theta1'; % (1 * 401) * (401 * 25) = (1 * 25)
  a2 = sigmoid(z2); % (1 * 25)
  a2 = [ones(1,1),a2]; %add 1 bias unit to front of a2, (1 * 26)
  z3 = a2*Theta2'; %(1 * 26) * (26 * 10) = (1 * 10)
  a3 = sigmoid(z3);% (1 * 10)
  actY = (y(i) == c);% (1 * 10)
  delta3 = a3-actY; % (1 * 10) - (1 * 10) = (1 * 10)
  delta2 = (delta3*Theta2)(2:end).*sigmoidGradient(z2); %   (1 * 25) .* (1 * 25) = (1 * 25)
  Theta2_grad = Theta2_grad + delta3' * a2; % (10 * 26) + [(10 * 1) * (1 * 26)] = (10 * 26)
  Theta1_grad = Theta1_grad + delta2' * a1; % (25 * 401) + [(25 * 1) * (1 * 401)] = (25 * 401)
end;
Theta2_grad = Theta2_grad/m;
Theta1_grad = Theta1_grad/m;

% Part 3: Implement regularization with the cost function and gradients.
%
%         Hint: You can implement this around the code for
%               backpropagation. That is, you can compute the gradients for
%               the regularization separately and then add them to Theta1_grad
%               and Theta2_grad from Part 2.
%

%replace weight of bias unit with 0 so as not to regularize bias unit
Theta2WithZero = [zeros(rows(Theta2),1),Theta2(:,2:end)]; 
Theta1WithZero = [zeros(rows(Theta1),1),Theta1(:,2:end)]; 

Theta2_grad = Theta2_grad + (lambda/m)*[zeros(rows(Theta2),1),Theta2(:,2:end)] ;
Theta1_grad = Theta1_grad + (lambda/m)*[zeros(rows(Theta1),1),Theta1(:,2:end)] ;


















% -------------------------------------------------------------

% =========================================================================

% Unroll gradients
grad = [Theta1_grad(:) ; Theta2_grad(:)];


end
