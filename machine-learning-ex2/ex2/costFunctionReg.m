function [J, grad] = costFunctionReg(theta, X, y, lambda)
%COSTFUNCTIONREG Compute cost and gradient for logistic regression with regularization
%   J = COSTFUNCTIONREG(theta, X, y, lambda) computes the cost of using
%   theta as the parameter for regularized logistic regression and the
%   gradient of the cost w.r.t. to the parameters. 

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta.
%               You should set J to the cost.
%               Compute the partial derivatives and set grad to the partial
%               derivatives of the cost w.r.t. each parameter in theta

GRADVECTOR = zeros(m,1);
regularizeTheta = theta(2:end);
regularizedComponent = (lambda * (regularizeTheta' * regularizeTheta))/(2*m);

%Working code without vectorization
%for i=1:m
    %hFunc = sigmoid(X(i,:)*theta);
    %fprintf('hFunc %f',hFunc);
    %GRADVECTOR(i) = hFunc - y(i);
    %fprintf('gradvector %f',hFunc);
    %J = J-(y(i)*log(hFunc) + (1-y(i))*log(1-hFunc))        
    %fprintf('J %f',J);
%end 
%J is the cost
%J= J/m + regularizedComponent;
%grad is the gradient vector
%grad = (X' * GRADVECTOR)/m + ((lambda/m) * [0;regularizeTheta] );


%Working code with vectorization
hFunc = sigmoid(X*theta);
GRADVECTOR = hFunc - y;
J = (J - (y'*log(hFunc) + (1-y)'*log(1-hFunc)))/m + regularizedComponent;
grad = (X' * GRADVECTOR)/m + ((lambda/m) * [0;regularizeTheta] );




% =============================================================

end
