function [J, grad] = costFunction(theta, X, y)
%COSTFUNCTION Compute cost and gradient for logistic regression
%   J = COSTFUNCTION(theta, X, y) computes the cost of using theta as the
%   parameter for logistic regression and the gradient of the cost
%   w.r.t. to the parameters.

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta.
%               You should set J to the cost.
%               Compute the partial derivatives and set grad to the partial
%               derivatives of the cost w.r.t. each parameter in theta
%
% Note: grad should have the same dimensions as theta
%

GRADVECTOR = zeros(m,1);
hFunc = zeros(m,1);

%Working code without vectorization
%for i=1:m
    %hFunc = sigmoid(X(i,:)*theta);
    %fprintf('hFunc %f',hFunc);
    %GRADVECTOR(i) = hFunc - y(i);
    %fprintf('gradvector %f',hFunc);
    %J = J-(y(i)*log(hFunc) + (1-y(i))*log(1-hFunc))        
    %fprintf('J %f',J);
%end 
%J = J/m;
%grad = (X' * GRADVECTOR)/m;


%Working code with Vectorization
hFunc = sigmoid(X*theta);
GRADVECTOR = hFunc - y;
J = (J - (y'*log(hFunc) + (1-y)'*log(1-hFunc)))/m;
grad = (X' * GRADVECTOR)/m;




% =============================================================

end
