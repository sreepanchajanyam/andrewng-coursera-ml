function [X_norm, mu, sigma] = featureNormalize(X)
%FEATURENORMALIZE Normalizes the features in X 
%   FEATURENORMALIZE(X) returns a normalized version of X where
%   the mean value of each feature is 0 and the standard deviation
%   is 1. This is often a good preprocessing step to do when
%   working with learning algorithms.

% You need to set these values correctly
X_norm = X;
mu = zeros(1, size(X, 2)); %mu is vector of means of each feature
sigma = zeros(1, size(X, 2)); %sigma is vector of standard deviation of each feature

% ====================== YOUR CODE HERE ======================
% Instructions: First, for each feature dimension, compute the mean
%               of the feature and subtract it from the dataset,
%               storing the mean value in mu. Next, compute the 
%               standard deviation of each feature and divide
%               each feature by it's standard deviation, storing
%               the standard deviation in sigma. 
%
%               Note that X is a matrix where each column is a 
%               feature and each row is an example. You need 
%               to perform the normalization separately for 
%               each feature. 
%
% Hint: You might find the 'mean' and 'std' functions useful.
%       



numberOfCols = size(X,2); % find out number of dimensions in the feature vector
for i = 1:numberOfCols % iterate over each dimension
    mu(i) = mean(X(:,i)); % calculate mean of each dimension for all the training examples
    sigma(i) = std(X(:,i)); % calculate standard deviation of each dimension for all the training examples
    X_norm(:,i) = (X(:,i) - mu(i))/sigma(i); % normalize/ feature scale training example matrix using the feature scaling
end;









% ============================================================

end
